package com.zuitt.example;

public class Parent {
    // mini-activity
    // Static Polymorphism with 2 greet method
        // first method-empty parameter, print a message saying "Hello friend!"
        // second method- with parameter, message will be Good +timeOfTheDay + name
        // run this on Main.java, send your output in Hangouts
    public void speak(){
        System.out.println("I am the parent");
    }
}
