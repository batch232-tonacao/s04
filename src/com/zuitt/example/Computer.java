package com.zuitt.example;
// implement Actions.java in Computer and instantiate in Main.java, run the methods.
// send your screenshot of your terminal output in Hangouts
public class Computer implements Actions {
    public void sleep(){
        System.out.println("Windows is hibernating");
    }

    public void run(){
        System.out.println("Windows is running");
    }

}
